# Author: Sthorn-Nibrel
# Date  : 22/10/2021

CC = gcc

EXEC = output/wordcount

# Project directory

SRCDIR   = src
OBJDIR   = object
OUTPUT   = output
BINDIR   = output

SOURCES  := $(wildcard $(SRCDIR)/*.c)
#INCLUDES := $(wildcard $(SRCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

all: $(EXEC)

$(EXEC):$(OBJECTS) 
	@$(CC) -o $(EXEC) $(OBJECTS)
	@echo "Linking complete!"


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) -o $@ -c $<
	@echo "Compilation complete!"

clear:
	@rm -rf $(OBJECTS)/*.o
	@rm -rf $(OUTPUT)/*
	@echo "Cleanup complete!"