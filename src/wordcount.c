#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wordcount.h"


char   *readStream(FILE * myFile) {
    unsigned int max= 128, size = 128;
    char   *buffer = (char *) malloc(max);

    if (buffer != NULL) { 
        int     ch = EOF;
        int     pos = 0;

        while ((ch = fgetc(myFile)) != '\n' && ch != EOF && !feof(myFile)) {
            buffer[pos++] = ch;
            if (pos == size) { 
                size = pos + max;
                buffer = (char *) realloc(buffer, size);
            }
        }
        buffer[pos] = '\0';
    return buffer;
    }
}


listWord addWord(listWord list, char *word)
{
    element *p_element = malloc(sizeof(element));

    p_element->word = malloc(sizeof(word));
    strcpy(p_element->word, word);
    p_element->nb_occurence = 1;
    p_element->next_word = list;
    return p_element;
}



void showListe(listWord list) {
    element *tmp = list;
    while (tmp != NULL) {
        printf("%d \t %s", tmp->nb_occurence, tmp->word);
        tmp = tmp->next_word;
    }
    int     total = totalWords(list);
    printf("%d \t total words\n", total);
}


listWord compareElements(listWord list, char *word) {
    element *tmp = list;

    while (tmp != NULL) {
        if (strcmp(tmp->word, word) == 0) {

            tmp->nb_occurence++;
            return list;
        }
        tmp = tmp->next_word;
    }
    listWord   newElement = addWord(list, word);
    return newElement;
}

int totalWords(listWord list) {
    element *tmp = list;
    int     totalWords = 0;
    while (tmp != NULL) {
        totalWords += tmp->nb_occurence;
        tmp = tmp->next_word;
    }
    return totalWords;
}

listWord ReadDictfile(const char *firstArg) {
    listWord   tmp = NULL;
    FILE   *fp = NULL;
    char   *word = NULL;
    char   *p = NULL;
    if (strcmp(dictfile, firstArg) == 0) {
#ifdef GETLINE
        char   *line = NULL;
        size_t  len = 0;
        ssize_t read;

        fp = fopen(dictfile, "r");
        if (fp == NULL)
            exit(EXIT_FAILURE);
        while ((read = getline(&line, &len, fp)) != -1) {
            if (line[0] != '#' && line[0] != '\n') {
                line[sizeof(line) - 1] = '\0';
                printf("%zu\n", len);
                tmp = compareElements(tmp, line);
            }
        }
        if (line)
            free(line);
        fclose(fp);
#elif FGETS
        fp = fopen(dictfile, "r");
        char    line[LIMIT_STRING];
        while (fgets(line, sizeof(line), fp)) {
            if (line[0] != '#' && line[0] != '\n') {
                tmp = compareElements(tmp, line);
            }
        }
        fclose(fp);
#endif
    }
    else {
        word = readStream(stdin);
        p = strtok(word, " ");
        for (int i = 0; p != NULL; i++) {
            tmp = compareElements(tmp, p);
            p = strtok(NULL, " ");
        }
        if (word) {
            free(word);
        }
    }
    return tmp;
}
