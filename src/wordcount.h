#ifndef H_WORDCOUNT
#define H_WORDCOUNT

#include <stdio.h>
#include <stdlib.h>

static const char dictfile[9] = "c\0";

/**
 * Definition of a structured list which will contain a word and the number of times it repeats
 */
typedef struct element{
    char *word;
    int nb_occurence;
    struct element *next_word;
}element;

/**
 * Definition of a chained list
 */
typedef element *listWord;

/**
 * This methode allows use to add a new element in a linked list
 * @param A ListWord  list that add tthe stringg new_word
 * @return A listWord with new element
 */
listWord  addWord(listWord list, char *new_word);

/**
 * 
 * @param 
 */
listWord compareElements(listWord list, char *word);

/**
 * This methode display all data 
 * @param ListWord myListto Show
 */
void showListe(listWord myList);

/**
 * This methode compare string with th last element of the linked list and add it if doesn't exist 
 * @param listWord list, char *string
 */
void compareword(listWord list, char *string);

/**
 * @param listWord list 
 * @return the number of élément
 */
int totalWords(listWord list);

/**
 * The methode read file and realocate automatically the memory
 * @param  A file pointer that goes and read
 */
char *readWord(FILE * stream);


/**
 * This method allows us to read the dictfile file while respecting the instructions of the statement,
 * in particular ignore line breaks and the line starting with #
 * @param fileName name of the dictile that read.
 */
listWord  ReadDictfile(const char *fileName);



#endif