# test competance Intersec group

<p align="left">
  <img height="400px" width="500px" style="text-align: center;"
   src="https://fr.wikipedia.org/wiki/Intersec_Group#/media/Fichier:INTERSEC_HD_JPG.jpg">
</p>


## Somaire

- [project structure](#structure)
- [solutions](#solutions)
- [How to Use code](#Use)

<a name="structure"></a>

## (2) project structure
This project consists of three folder a src folder where we find all the source code (c and h file)
an object file where we will find all the object files (main.o wordcount.o) and an output file where we find the executable

---

<a name="solutions"></a>

## (3) solutions
The solution chosen to solve this problem is a fairly intuitive approach and therefore which is not optimized.
it is simply a matter of browsing the file or standard input data, browsing line by line and character by character
and to store in a chain list by incrementing the occurrence of the words to measure. and finally display the result as requested in the statement.

---

<a name="Use"></a>

## (4) How to Use code
* Compilation 
  to compile the project, simply go to the root of the project and type:
  - ```make clean```
  - ```make```
* Utilisation
  Go to the output folder where the wordcount file is located et saisir sur votre terminal
  - ```wordcount dictfile```  ou 
  - ```./wordcount dictfile``` avec dictfile le nom du fichier à analyser
  
<strong> This program is not very efficient and requires several optimization only works for files with one word per line </strong> 

---
### Author:

Name: Sthorn-Nibrel Bemouela <br>
[linkdin](https://www.linkedin.com/in/sthorn-nibrel-bemouela-systeme-embarque-electronique)
